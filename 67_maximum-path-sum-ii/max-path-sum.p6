use v6;
use Graph;

my $head;

for 'triangle.txt'.IO.lines -> $line {
    state @last-row;
    my @words = $line.words>>.Int;
    my @this-row = @words.map: { new Graph::Simple::Node: data => $_ };

    if @last-row {
        for @last-row.kv -> $i, $e {
            $e.left = @this-row[$i];
            $e.right = @this-row[$i + 1];
        }
    } else { $head = @this-row[0] }

    @last-row = @this-row;
}

$HEAD.&find-max-sum.say;

