use v6;
unit module Graph;

class Graph::Simple is rw {*}

class Graph::Simple::Node is rw {*}

class Graph::Simple is export is rw {
    has Graph::Simple::Node $.head;
}

class Graph::Simple::Node is export is rw {
    has Int $.data;
    has Graph::Simple::Node $.left;
    has Graph::Simple::Node $.right;
}

sub find-max-sum(Graph::Simple::Node $node) is export {
    return 0 unless $node.defined;
    return $node.data unless $node.left and $node.right;

    my $left-sum = find-max-sum $node.left;
    my $right-sum = find-max-sum $node.right;

    $node.data + max $left-sum, $right-sum;
}
