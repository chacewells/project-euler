#include <stdlib.h>
#include <stdio.h>

struct Node {
    int data;
    struct Node *left;
    struct Node *right;
};

int findMaxSum(struct Node *node) {
    if (node == NULL) {
        return 0;
    }

    if (node->left == NULL && node->right == NULL) {
        return node->data;
    }

    int leftSum = findMaxSum( node->left );
    int rightSum = findMaxSum( node->right );

    if ( leftSum > rightSum ) {
        return node->data + leftSum;
    } else {
        return node->data + rightSum;
    }
}
