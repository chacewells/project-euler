#!/usr/bin/perl
use List::Util 'sum';
use 5.018;

my $count;
OUTER:
for (my $inx = 997; $inx > 900; $inx -= $inx%10 == 7 ? 4 : 6) {
    for (my $jnx = 997; $jnx > 900; $jnx -= $jnx%10 == 7 ? 4 : 6) {
        next unless $inx%10 == $jnx%10;
        $count++;
        my $prod = $jnx*$inx;
        &print_info($prod, $inx, $jnx) and last OUTER
            if &is_pal($prod);
    }
}
say "total iterations: $count";

sub print_info {
    my ($pal, $one, $two) = @_;
    print "factors: $one, $two\n";
    print "largest: $pal\n";
}

sub is_pal {
    my @num_chars = split('', ''.$_[0]);
    my $base = 10;
    my $sum = sum map { $num_chars[$_] * ($base**$_) } (0..$#num_chars);
    $sum == $_[0]
}
