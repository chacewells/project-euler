function isPalendrome(n) {
    var str_n = String(n).split('');
    var k = str_n.length;
    var b = 10;
    var sum = 0;
    for (var i = 0; i < k; ++i) {
        var a = str_n[i];
        sum += a * Math.pow(b, i);
    }
    return sum == n;
}
