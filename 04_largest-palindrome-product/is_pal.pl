#!/usr/bin/perl

sub is_pal {
    my @num_chars = split('', ''.$_[0]);
    my($b,$sum) = (10);
    foreach my $i (0..$#num_chars) {
        my $a = $num_chars[$i];
        $sum += $a * ($b**$i);
    }
    $sum == $_[0];
}

printf("%d\n", &is_pal($_)) while <>;
