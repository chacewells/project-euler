v6.d;

sub fact($n) {
    [*] 1..$n
}

sub bin-coeff($n, $k) {
    fact($n) / ( fact($k) * fact($n - $k) )
}

sub lattice-paths($n, $k) {
    bin-coeff $n + $k, $n
}

multi MAIN(Int $n = 20, Int $k = 20) {
    .say for :$n, :$k;
    my $lattice-paths = lattice-paths $n, $k;
    :$lattice-paths.say;
}

