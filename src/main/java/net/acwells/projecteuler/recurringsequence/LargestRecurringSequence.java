package net.acwells.projecteuler.recurringsequence;

import java.util.HashMap;

/**
 * Project Euler #26 - Reciprocal Cycles
 */
public class LargestRecurringSequence {
    public static void main(String[] args) {
        int d = 1;
        int maxSequenceLength = 0;
        int dMaxSequence = d;
        String maxSequence = "";

        for (;d < 1_000; ++d) {
            String sequence = fractionToDecimal(d);
            if (sequence.length() > maxSequenceLength) {
                maxSequenceLength = sequence.length();
                dMaxSequence = d;
                maxSequence = sequence;
            }
        }

        System.out.println("d = " + dMaxSequence + "; length = " + maxSequenceLength + "; sequence = " + maxSequence);
    }

    static String fractionToDecimal(int denr)
    {
        final int numr = 1;
        // Initialize result
        String res = "";

        // Create a map to store already
        // seen remainders. Remainder is
        // used as key and its position in
        // result is stored as value.
        // Note that we need position for
        // cases like 1/6.  In this case,
        // the recurring sequence doesn't
        // start from first remainder.
        var mp = new HashMap<Integer, Integer>();
        mp.clear();

        // Find first remainder
        int rem = numr % denr;

        // Keep finding remainder until
        //  either remainder becomes 0 or repeats
        while ((rem != 0) && (!mp.containsKey(rem)))
        {
            // Store this remainder
            mp.put(rem, res.length());

            // Multiply remainder with 10
            rem = rem * 10;

            // Append rem / denr to result
            int res_part = rem / denr;
            res += String.valueOf(res_part);

            // Update remainder
            rem = rem % denr;
        }

        if (rem == 0) {
            return "";
        } else if (mp.containsKey(rem)) {
            return res.substring(mp.get(rem));
        }

        return "";
    }
}
