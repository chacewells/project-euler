package net.acwells.projecteuler.thousanddigitfib;

import java.math.BigInteger;

/**
 * Euler #25
 */
public class ThousandDigitFibonacciNumber {
    public static void main(String[] args) {
        BigInteger prev, curr;
        prev = curr = new BigInteger("1");

        while (curr.toString().length() < 1_000) {
            BigInteger temp = curr.add(prev);
            prev = curr;
            curr = temp;
        }

        System.out.println("Thousand digit fib = " + curr);
    }
}
