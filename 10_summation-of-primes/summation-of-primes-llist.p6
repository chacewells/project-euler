use v6.c;

class PrimeNode {
    has PrimeNode $.next is rw;
    has int $.value;
}

my PrimeNode $cache .= new: value => 2;
my PrimeNode $tail = $cache;

loop (my int $x = $cache.value; $x < 2_000_000; $x = next-prime) {
    say $x;
}

my $sum;
loop (my $itr = $cache; $itr.defined && $itr.value < 2_000_000; $itr.=next) {
    $sum += $itr.value;
}
say "sum of primes less than 2,000,000: $sum";

sub next-prime returns int {
    my int $n = $tail.value;
    if $n == 2 {
        return add-to-cache 3;
    }

    loop (my int $x = $n + 2; !is-prime($x); $x += 2) {}

    return add-to-cache $x;
}

sub is-prime(int $x) returns Bool {
    loop (my PrimeNode $iterator = $cache; $iterator.next.defined; $iterator.=next) {
        my int $divisor = $iterator.value;
        return False if $x %% $divisor;
    }

    return True;
}

sub add-to-cache(int $prime) returns int {
    $tail.next .= new: value => $prime;
    $tail.=next;
    return $prime;
}
