use v6.c;

my @primes = (2..2_000_000).grep(*.is-prime);

@primes.sum.say;

say now - INIT now;
