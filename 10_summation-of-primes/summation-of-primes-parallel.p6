use v6.c;

sub sieve ($n, $i) { $i ** 2, * + $i ...^ * > $n }

my Int $n = 2_000_000;
my Int $sqrt-n = $n.sqrt.Int;

my Set $series = set 2 .. $n;
my Set $sub-series = set 2 .. $sqrt-n;

my Set $sub-dross = set flat map &sieve.assuming($sqrt-n), 2 .. $sqrt-n.sqrt.Int;

my @sub-primes = keys $sub-series (-) $sub-dross;

my Set $dross = set flat map &sieve.assuming($n), @sub-primes;

($series (-) $dross).keys.sum.say;
