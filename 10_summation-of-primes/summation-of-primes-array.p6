use v6.c;

my $n = 2_000_000;
my @a = 2..$n;
my Bool @sieve = True xx ($n-1);

for 2 .. $n.sqrt.Int -> $i {
    if @sieve[$i] {
        my @j = $i ** 2, * + $i ... * > $n;
        @sieve[@j] = False xx @j;
    }
}

@sieve.pairs[2..*].grep(*.value)>>.key.sum.say;
