#include <stdio.h>
#include <stdlib.h>

struct PrimeNode {
    struct PrimeNode *next;
    int value;
};

struct PrimeNode *cache;
struct PrimeNode *tail;

void initialize_cache();
int next_prime();
char is_prime(int prime_candidate);
void add_to_cache(int prime);

int current_prime;

int main(void) {
    initialize_cache();

    for (current_prime = tail->value; current_prime < 10000; current_prime = next_prime())
        printf("%d\n", current_prime);
}

void initialize_cache() {
    cache = (struct PrimeNode *) malloc(sizeof(struct PrimeNode));
    cache->value = 2;
    tail = cache;
}

int next_prime() {
    int prime_candidate = tail->value;
    if (2 == prime_candidate) {
        add_to_cache(3);
        return 3;
    }
    
    for (;
        !is_prime(prime_candidate);
        prime_candidate += 2);
    
    add_to_cache(prime_candidate);
    return prime_candidate;
}

char is_prime(int prime_candidate) {
    struct PrimeNode *iterator = cache;
    do {
        if (0 == (prime_candidate % iterator->value) ) {
            return 0;
        }
    } while ( NULL != (iterator = iterator->next) );

    return 1;
}

void add_to_cache(int prime) {
    struct PrimeNode *next = (struct PrimeNode *) malloc(sizeof(struct PrimeNode));
    next->value = prime;
    next->next = NULL;
    tail->next = next;
    tail = next;
}
