use v6.c;

constant \cache-file = 'prime-cache.txt'.IO;
say "cache-file: {cache-file.Str}";

my Int @cache = do given cache-file {
    when *.f { .lines>>.Int }
    default { [2] }
};

signal(SIGINT).tap: {
    spurt cache-file: @cache.fmt('%d', "\n");
    exit;
}

loop (my Int $x = next-prime; $x < 2_000_000; $x = next-prime) {
    $x.say;
}

say now - INIT now;

my $sum = @cache[1 ..^ @cache.end].sum; # exclude the one over 2 million
say "sum of primes less than 2 million: $sum";

sub next-prime {
    my $n = @cache[*-1];
    if $n == 2 {
        push @cache, 3;
        return 3;
    }

    loop (my Int $x = $n + 2, my Bool $prime = False ;; $x += 2) {
        loop (my Int $i = 1; $i < @cache.elems; ++$i) {
            my $divisor = @cache[$i];
            if $x %% $divisor {
                last;
            }

            $prime = True;
        }
        if $prime {
            push @cache, $x;
            return $x;
        }
    }
}

