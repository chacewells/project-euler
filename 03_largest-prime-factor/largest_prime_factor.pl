#!/usr/bin/perl

$num = 600851475143;
$half = $num / 2;

foreach (2..$half) {
    if ( $num % $_ == 0) {
        print "smallest prime: $_\n";
        print "largest prime: ", $num / $_, "\n";
        last;
    }
}
