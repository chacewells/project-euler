#!/usr/bin/perl
use 5.018;
use POSIX;
use Time::Local;

my $start = timelocal(localtime);
foreach (grep $_ % 2, 1..10_181) { say if is_prime($_) }
my $end = timelocal(localtime);
print "total: ", $end - $start;

sub is_prime {
    state %primes;
    state @primes;
    my $num = floor(shift);
    my $half = floor($num / 2);

    if ( $primes{$num} || $num <= 1 || $num == 2)
    { !undef }
    else {
        my $largest = 2;
        foreach my $current (@primes) {
            return !!undef if $num % $current == 0;
            $largest = $current if $current > $largest;
        }
        foreach my $current ( ($largest + 1)..$half) {
            return !!undef if $num % $current == 0;
        }
        $primes{$num} = 1;
        !undef
    }
}
