use v6.c;
use Triangle;

for 1..1_000_000 -> \i {
    my \i-triangle = triangle i;
    my \i-triangle-divisors = count-divisors i-triangle;

    printf "%d : %d : %d\n", i, i-triangle, i-triangle-divisors;
    last if i-triangle-divisors > 500;
}
