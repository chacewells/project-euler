import static java.lang.Math.floor;
import static java.lang.Math.sqrt;

public class Triangle {

    public static void main(String[] args) {
        for (int i = 1; i <= 1000000; ++i) {
            int iTriangle = triangle(i);
            int iTriangleDivisors = countDivisors(iTriangle);

            System.out.printf("%d : %d : %d\n", i, iTriangle, iTriangleDivisors);
            if (iTriangleDivisors > 500) { break; }
        }
    }

    static int triangle(int n) {
        int sum = 0;
        for (int i = 1; i <= n; ++i) {
            sum += i;
        }

        return sum;
    }

    static int countDivisors(int n) {
        int divisorCount = 2;
        int sqrtN = (int) floor( sqrt( (double) n ) );

        for (int x = 2; x <= sqrtN; ++x) {
            if ( n % x == 0 ) {
                int y = n / x;
                ++divisorCount;

                if ( y != x ) {
                    ++divisorCount;
                }
            }
        }

        return divisorCount;
    }
}
