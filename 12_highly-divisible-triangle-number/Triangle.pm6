use v6;
use experimental :cached;
unit module Triangle;

our &triangle is export = { sum 1 .. $_ };

our @triangles is export = map &triangle, 1 .. *;

sub divisors(Int $n) is export {
    (1 .. $n.sqrt.floor).grep(-> $x {
        $n %% $x ?? ($x, $n / $x).Slip !! ().Slip
    }).&set;
}

sub count-divisors(Int \n) is export {
    my $divisor-count = 2;
    for 2 .. n.sqrt.floor -> \x {
        if n %% x {
            my \y = n / x;
            ++$divisor-count;
            ++$divisor-count if y != x;
        }
    }

    $divisor-count;
}

sub divisor-crawl(Range $r) is export {
    for @$r {
        my $triangle = .&triangle;
        my $divisors = $triangle.&divisors.elems;
        "$_ : $triangle : $divisors".say;
        last if $divisors > 500;
    }
}
