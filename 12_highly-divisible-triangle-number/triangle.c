#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int triangle(int n);
int countDivisors(int n);

int main(void) {
    for (int i = 1; i <= 1000000; ++i) {
        int iTriangle = triangle(i);
        int iTriangleDivisors = countDivisors(iTriangle);

        printf("%d : %d : %d\n", i, iTriangle, iTriangleDivisors);
        if (iTriangleDivisors > 500) { break; }
    }

    return 0;
}

int triangle(int n) {
    int sum = 0;
    for (int i = 1; i <= n; ++i) {
        sum += i;
    }

    return sum;
}

int countDivisors(int n) {
    int divisorCount = 2; // one for 1 and one for n
    int sqrtN = (int) sqrt( (double) n );

    for (int x = 2; x <= sqrtN; ++x) {
        if ( n % x == 0 ) {
            int y = n / x;
            ++divisorCount; // one for x

            if ( y != x ) { // another for y
                ++divisorCount;
            }
        }
    }

    return divisorCount;
}
