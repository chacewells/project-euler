use v6;

# overloading string 'x' operator
multi infix:<x> (Int \a, Int \b) { a * b }

# but we still want the string 'x' operator
multi infix:<x> (Str \a, Int \b where * > 1) { [~] gather for ^b { take a } }

multi postfix:<!> (1) { 1 }
multi postfix:<!> (Int \n where * > 1) { n x (n - 1)! }

my $hundred-fact-str = (100!).Str;
my @hundred-fact-digits = $hundred-fact-str.split('');

my $sum = @hundred-fact-digits.sum;

say "100! = ($hundred-fact-str). sum of digits: $sum";
