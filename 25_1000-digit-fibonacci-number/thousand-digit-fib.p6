use v6.c;

my constant \fibs = 0, 1, 1, * + * ... *;
my &thousand-digit = *.value.chars == 1_000;
my $first-thousand = fibs.pairs.first: &thousand-digit;

$first-thousand.say;
