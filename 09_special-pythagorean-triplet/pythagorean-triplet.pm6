unit module pythagorean-triplet;
use v6;

our &is-triplet is export = * ** 2 + * ** 2 == * ** 2;

constant \MAX-A = 332;

sub find-thousand-sum-triplet is export {
    for (1..332).reverse -> $a {
        my $a2 = $a ** 2;
        for $a + 1 .. 1000 -> $b {
            my $b2 = $b ** 2;
            my $c2 = $a2 + $b2;
            my $c = $c2.sqrt;
            next unless $c %% 1;
            if $a + $b + $c == 1_000 {
                say "$a^2 + $b^2 = $c^2";
                say "$a2 + $b2 = $c2";
                say "$a + $b + $c = {$a + $b + $c}";
                say "$a * $b * $c = {$a * $b * $c}";
                return;
            }
        }
    }
}
