use v6;

my constant %char-weight = Hash.new: |( 'A' .. 'Z' Z=> 1 .. 26 );

my @names = 'names.txt'.IO.lines[0].split(',')>>.subst('"', '', :g).sort;
my %names = @names Z=> 1 .. *;

my $total-name-score = %names.pairs.map(&calc-name-score).sum;
$total-name-score.fmt("total names score: %d").say;

sub calc-name-score($name-pos) {
    my @char-weights = grep ?*, %char-weight{ $name-pos.key.split('') }:v;
    [+] |@char-weights, $name-pos.value;
}
