use v6;
use Collatz;

class CollatzCandidate { has ($.start, $.chain-length) }

# my @candidates = map { new CollatzCandidate: start => $_, chain-length => .&determine-chain-length }, 1 .. 1_000_000;
for 1 .. 1_000_000 {
    "$_ : &determine-chain-length($_)".say
}
