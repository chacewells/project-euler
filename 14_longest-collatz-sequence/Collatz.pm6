use v6;
unit module Collatz;

sub collatz($x --> Seq) is export {
    $x, -> $n { $n %% 2 ?? $n / 2 !! 3 * $n + 1 } ... *
}

sub extract-chain(Seq $s --> Seq) is export {
    gather for @$s {
        .take;
        last if $_ == 1;
    };
}

sub determine-chain-length($x) is export {
    $x.&collatz.&extract-chain.elems
}
