#include <stdlib.h>
#include <stdio.h>

struct Collatz {
    long start;
    long chainLength;
};

long collatzLength(long n);
void findMaxCollatzChain(long startHigh, struct Collatz *currentMax);

long main(void) {
    struct Collatz *maxCollatz = (struct Collatz *) malloc(sizeof(struct Collatz));
    findMaxCollatzChain(1000000, maxCollatz);
    printf("%ld : %ld\n", maxCollatz->start, maxCollatz->chainLength);
    return 0;
}


long collatzLength(long n) {
    long currentCollatz = n;
    long collatzLength = 0;

    while (1) {
        ++collatzLength;
        if (currentCollatz % 2 == 0) {
            currentCollatz = currentCollatz / 2;
        } else {
            currentCollatz = (3 * currentCollatz) + 1;
        }

        if (currentCollatz == 1) break;
    }

    return collatzLength;
}

void findMaxCollatzChain(long startHigh, struct Collatz *currentMax) {
    currentMax->start = 1;
    currentMax->chainLength = 1;
    for (long i = 2; i <= startHigh; ++i) {
        struct Collatz current = { start: i, chainLength: collatzLength(i) };

        if (current.chainLength > currentMax->chainLength) {
            currentMax->start = current.start;
            currentMax->chainLength = current.chainLength;
        }
    }
}
