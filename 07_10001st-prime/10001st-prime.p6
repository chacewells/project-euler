use v6.c;

constant \cache-file = 'prime-cache.txt';
say "cache-file: {cache-file}";

my Int @cache = do given cache-file.IO {
    when *.f { .lines».Int }
    default { [] }
};
say "initial cache: @cache[]";

signal(SIGINT).tap: {
    spurt cache-file, @cache.fmt('%d', "\n");
    exit;
}

for @cache.end … 10_0001 -> $x {
    say "$x: &next-prime()";
}

sub next-prime {
    return @cache = 2 unless @cache;

    my $n = @cache[*-1];
    
    if $n == 2 {
        push @cache, 3;
        return 3;
    }

    my $divisors = any @cache[1..^*];
    for $n + 2, * + 2 … * -> $x { # iterate over odds
        unless $x %% $divisors {
            push @cache, $x;
            return $x;
        }
    }
}

