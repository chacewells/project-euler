use v6;

sub is-amicable(Int $n --> Bool) {
    my @divisors-of-n = proper-divisors $n;
    my $o = @divisors-of-n.sum;
    my @divisors-of-o = proper-divisors $o;
    my $maybe-n = @divisors-of-o.sum;

    $maybe-n == $n;
}

sub proper-divisors(Int $n --> Array of Int) {
    my Int @divisors;
    my $sqrt-n = Int(sqrt $n);
    for 1..$sqrt-n -> $x {
        if $n %% $x {
            my $y = Int($n / $x);
            push @divisors, $x;
            push @divisors, $y if $y != $x and $y < $n;
        }
    }

    @divisors;
}

my $amicable-sum = sum grep &is-amicable, 1 .. 10_000;

$amicable-sum.fmt("sum of amicable numbers less than 10,000: %d").say;
