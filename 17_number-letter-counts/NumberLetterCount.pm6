unit module NumberLetterCount;

my constant @ones-words = '', |<one two three four five six seven eight night>;
my constant @tens-words = '', '', |<twenty thirty forty fifty sixty seventy eighty ninety>;
my constant @teens = <ten eleven twelve thirteen fourteen fifteen sixteen seventeen eighteen nineteen>;

sub to-words(Int $n --> Str) is export {
    my $thousands = Int($n / 1000);
    my $hundreds = Int( $n % 1000 / 100 );
    my $tens = Int( $n % 100 / 10 );
    my $ones = $n % 10;

    my $result = '';

    if ?$thousands {
        $result ~= "@ones-words[$thousands] thousand";
    }

    if ?$hundreds {
        $result ~= ' ' if ?$thousands;
        $result ~= "@ones-words[$hundreds] hundred";
    }

    if ?$tens {
        $result ~= ' and ' if ?$thousands or ?$hundreds;
        if $tens > 1 {
            $result ~= @tens-words[$tens] ~ (?$ones ?? " @ones-words[$ones]" !! '');
        } elsif $tens == 1 {
            $result ~= @teens[$ones];
        }
    }

    $result;
}

