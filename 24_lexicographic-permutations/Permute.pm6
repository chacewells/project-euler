use v6;
unit module Permute;

multi sub generate(Int \n, Int @a) is export {
    @a.fmt(q{"%d"}, ', ').say, return if n == 1;
    for @a.keys -> \i {
        generate n - 1, @a;
        if n %% 2 {
            @a[i, n - 1] = @a[n - 1, i];
        } else {
            @a[0, n - 1] = @a[n - 1, 0];
        }
    }
    generate n - 1, @a;
}

multi sub generate(Int @a) is export {
    my \n = @a.elems;
    generate n, @a;
}
