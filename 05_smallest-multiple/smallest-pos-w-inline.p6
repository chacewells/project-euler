use v6;
use soft;
use Inline;
use NativeCall;

my Int @every = 1..20;
my $every-set = set @every;
my $primes = set 1,2,3,5,7,11,13,17,19;
my $non-primes = $every-set (-) $primes;
my $other-non-needed = set
     4 # factor of 12,16,20
    ,6 # factor of 12,18
    ,8 # factor of 16
    ,9 # factor of 18
    ,10# factor of 20
;
my CArray[int64] \divisors = CArray[int64].new(keys( $non-primes (-) $other-non-needed ));

my int64 $increment = [*] $primes.keys;
my int64 $max = [*] @every;
my int $divisors_length = divisors.elems;
say "inc: $increment";
say "max: $max";
say "len_cand: $divisors_length";
say "candidates: {divisors.Array}";

my int64 $result = find_least_common_multiple($increment, $max, divisors, $divisors_length);
my $start = now;
$result = find_least_common_multiple($increment, $max, divisors, $divisors_length);
my $time = now - $start;

say "found least common multiple  $result in $time";

sub find_least_common_multiple(
        int64 \increment,
        int64 \max,
        CArray[int64] \divisors,
        int32 \divisors_length
) is inline('C') returns int64 { Q:to/C/ }
    #include <stdio.h>
    DLLEXPORT long find_least_common_multiple(
            long increment,
            long max,
            long divisors[],
            int divisors_length) {
        long candidate, divisor;
        long result = -1;
        int divisor_index;
        char all_match;

        for (candidate = increment; candidate < max; candidate += increment)
        {
            all_match = 1;
            for (divisor_index = 0; divisor_index < divisors_length; ++divisor_index)
            {
                divisor = divisors[divisor_index];
                if (0 != candidate % divisor) // keep checking until one doesn't match
                {
                    all_match = 0;
                    break;
                }
            }
            
            if (1 == all_match)
            {
                // INNER completed, so record and return the result
                result = candidate;
                break;
            }
        }

        return result;
    }
C
