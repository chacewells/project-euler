use v6;

my Int @every = 1..20;
my $every-set = set @every;
my $primes = set 1,2,3,5,7,11,13,17,19;
my $non-primes = $every-set (-) $primes;
my $np-junc = $non-primes.keys.all;

my $inc = [*] $primes.keys;
my $max = [*] @every;

say "inc is $inc";
say "max is $max";
say "max possible steps: {$max/$inc}";

my $lcm;
my $start = now;
for $inc, * + $inc ... $max -> $val {
    if so $val %% $np-junc {
        $lcm = $val;
        last;
    }
}
my $time = now - $start;

say "found least common multiple $lcm in $time";
