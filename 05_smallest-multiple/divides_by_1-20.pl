#!/usr/bin/perl
use Number::Format;
# 2 2 2 2 => 2 4 8 16
# 3 3 => 3 6 9 12 18
# 5   => 20
# 7   => 14
# 11
# 13
# 17
# 19

$fmt = Number::Format->new(-thousands_sep => ',');
printf "%s\n", $fmt->format_number(2**4 * 3**2 * 5 * 7 * 11 * 13 * 17 * 19);
