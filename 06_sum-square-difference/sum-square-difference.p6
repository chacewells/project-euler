#!/usr/bin/env perl6
use v6;

my @first-ten     = 1..10;
my @first-hundred = 1..100;

my $sum-of-squares = [+] @first-hundred.&(** ** 2);
my $square-of-sum = 2 R** [+] @first-hundred;


print qq:to/END/;
{:@first-ten}
    sum of squares: &sum-of-squares(@first-ten)
     square of sum: &square-of-sum(@first-ten)
        difference: &difference(@first-ten)
{:@first-hundred}
    sum of squares: &sum-of-squares(@first-hundred)
     square of sum: &square-of-sum(@first-hundred)
        difference: &difference(@first-hundred)
END

sub sum-of-squares { [+] @^a.&(** ** 2) }
sub square-of-sum  { 2 R** [+] @^a }
sub difference { square-of-sum(@_) - sum-of-squares(@_) }
