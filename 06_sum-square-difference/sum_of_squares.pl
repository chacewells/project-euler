#!/usr/bin/perl
use List::Util qw(sum reduce);
use Number::Format;

@first_hundred = (1..100);
$sum_of_squares = sum map { $_**2 } @first_hundred;
$square_of_sums = $sum_of_squares + (reduce { $a+$b } map { 2*$_ } @first_hundred)**2;
$fmt = Number::Format->new(-thousands_sep => ',');
printf "%s\n", $fmt->format_number($sum_of_squares);
printf "%s\n", $fmt->format_number($square_of_sums);
printf "%s\n", $fmt->format_number($square_of_sums - $sum_of_squares);
