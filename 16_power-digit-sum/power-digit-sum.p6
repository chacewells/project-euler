use v6.d;

my $n = 2 ** 1000;
:$n.say;

my $sum = $n.Str.comb>>.Int.sum;
:$sum.say;
