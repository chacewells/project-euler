use v6;
unit module Abundant;

enum Abundance is export <DEFICIENT PERFECT ABUNDANT>;

sub find-divisors(Int $x) is export {
    map map-divisor($x), 1 .. $x.sqrt.floor;
}

sub determine-abundance(Int $x --> Abundance) is export {
    my $sum-of-divisors = $x.&find-divisors.sum;
    given $sum-of-divisors cmp $x {
        when Less { DEFICIENT }
        when Same { PERFECT }
        when More { ABUNDANT }
    };
}

sub map-divisor($n) is export {
    -> $x {
        if $n %% $x {
            my $y = $n / $x;
            if $y !== $x | $n {
                ($x, $y).Slip
            } else { $x }
        } else { ().Slip }
    };
}
