use v6;
use Abundant;


our $non-abundant-range is export = set 1 .. 28_123;
our $abundant is export = set grep { .&determine-abundance ~~ ABUNDANT }, $non-abundant-range.keys;
our %work = %$non-abundant-range;

%work{$_}:delete for $abundant.keys X+ $abundant.keys;

%work.keys.sum.fmt('Sum of positive integers that cannot be expressed as the sum of 2 abundant numbers: %d').say;
